#Author: facchini@gmail.com

Feature: Planning a trip
  I want to find possible trip paths between two locations so I can decide what is the best way to travel

  Scenario: A trip request can be executed and results returned
		Given Phileas is planning a trip
		When he executes a trip plan from "North Sydney Station" to "Town Hall Station"
		Then a list of trips should be provided

