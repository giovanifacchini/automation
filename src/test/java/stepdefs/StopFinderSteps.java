package stepdefs;

import java.io.FileReader;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import api.stopFinder.StopFinder;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class StopFinderSteps {
	
	private StopFinder stopFinderAPI;
	private JSONObject response;
	private JSONObject stopFound;

	@Given("^Phileas is looking for a stop$")
	public void phileas_is_looking_for_a_stop() throws Throwable {
	    // initialize
	    Properties p = new Properties();
	    p.load(new FileReader("src/test/config/api.properties"));
	    
	    stopFinderAPI = new StopFinder(p.getProperty("url"));
	}

	@When("^he searches for \"([^\"]*)\"$")
	public void he_searches_for(String stopName) throws Throwable {
		response = stopFinderAPI.findByName(stopName);
	}

	@Then("^a stop named \"([^\"]*)\" is found$")
	public void a_stop_named_is_found(String stopFullName) throws Throwable {
		JSONArray locations = response.getJSONArray("locations");
		boolean found = false;
		try {
			for (int i=0;i<locations.length();i++) {
				if (locations.getJSONObject(i).get("name").toString().equalsIgnoreCase(stopFullName)) {
					found = true;
					stopFound = locations.getJSONObject(i);
				}
			}
		} catch (Exception e) {} //in case some malformed issue
		Assert.assertTrue("Could not find stop name ["+stopFullName+"] in the following full response: ["+response.toString()+"]", found);
	}

	@Then("^the stop provides more than one mode of transport$")
	public void the_stop_provides_more_than_one_mode_of_transport() throws Throwable {
		Assert.assertNotNull("No stop found", stopFound);
		boolean moreThan1Stop = stopFound.getJSONArray("modes").length() > 1 ? true : false;
		Assert.assertTrue("Stop does not have more than 1 mode of transport. Found ["
				+stopFound.getJSONArray("modes").length()+"]", moreThan1Stop);
	}

	
}
