package stepdefs;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.tripPlanner.TripPlanner;

public class TripPlannerSteps {
	
	private TripPlanner tripPlannerPage;
	private Properties p;
	
	public TripPlannerSteps() throws FileNotFoundException, IOException {
		p = new Properties();
	    p.load(new FileReader("src/test/config/trip.properties"));
	}

	@Given("^Phileas is planning a trip$")
	public void phileas_is_planning_a_trip() throws Throwable {
		tripPlannerPage = new TripPlanner(p.getProperty("driver"), p.getProperty("url"));
	}

	@When("^he executes a trip plan from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void he_executes_a_trip_plan_from_to(String from, String to) throws Throwable {
		tripPlannerPage.searchTrip(from, to);
	}

	@Then("^a list of trips should be provided$")
	public void a_list_of_trips_should_be_provided() throws Throwable {
	    Assert.assertNotEquals("Could not find trips for this search", 0, tripPlannerPage.getTripOptions());
	    tripPlannerPage.close();
	}
	
}
