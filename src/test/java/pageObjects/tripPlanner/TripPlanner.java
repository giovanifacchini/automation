package pageObjects.tripPlanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import seleniumFramework.PageActions;

public class TripPlanner {

	private WebDriver driver;
	private PageActions pageActions;
	
	private By fromInputField = By.id("search-input-From");
	private By suggestionsFromTable = By.id("suggestions-From");
	
	private By toInputField = By.id("search-input-To");
	private By suggestionsToTable = By.id("suggestions-To");
	
	private By goButton = By.id("search-button");
	private By listOfTravel = By.xpath("//div[@role='list']/div[@role='listitem']");
	
	public TripPlanner(String driverPath, String url) throws Exception {
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.get(url);
		pageActions = new PageActions(driver);
		if (!pageActions.waitForElement(fromInputField))
			throw new Exception("Page could not be loaded");
		
	}
	
	public void searchTrip(String from, String to) {
		writeTextToFieldSelectingFromTable(fromInputField, from, suggestionsFromTable, By.xpath("//div[text()='"+from+"']/ancestor::a"));
		writeTextToFieldSelectingFromTable(toInputField, to, suggestionsToTable, By.xpath("//div[text()='"+to+"']/ancestor::a"));
		pageActions.clickElement(goButton);
	}
	
	private void writeTextToFieldSelectingFromTable(By fieldLocator, String searchText, By tableLocator, By tableElementLocator) {
		pageActions.writeTextToField(fieldLocator, searchText);
		pageActions.waitForElement(tableLocator);
		pageActions.waitForElement(tableElementLocator);
		pageActions.clickElement(tableElementLocator);
	}
	
	public int getTripOptions() {
		if (!pageActions.waitForElement(listOfTravel))
			return 0;
		
		return pageActions.countElements( listOfTravel);
	}
	
	public void close() {
		driver.quit();
	}
}
