package seleniumFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageActions {
	
	private WebDriver d;
	
	public PageActions(WebDriver d) {
		this.d = d;
	}

	public void writeTextToField(By fieldLocator, String searchText) {
		WebElement field = d.findElement(fieldLocator);
		field.click();
		field.sendKeys(searchText);
	}
	
	public void clickElement(By element) {
		d.findElement(element).click();
	}
	
	public boolean waitForElement(By element) {
		WebDriverWait wait = new WebDriverWait(d, 20, 1000);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(element));
		} catch (TimeoutException e) {
			return false;
		}
		return true;
	}
	
	public int countElements(By search) {
		return d.findElements(search).size();
	}
}
