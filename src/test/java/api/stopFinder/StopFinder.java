package api.stopFinder;

import org.json.JSONObject;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class StopFinder {
	
	private String url;
	
	public StopFinder(String url) {
		this.url = url;
	}
	
	public JSONObject findByName(String stopName) throws UnirestException {
		return Unirest.get(url)
		  .queryString("TfNSWSF", "true")
		  .queryString("language", "en")
		  .queryString("name_sf", stopName)
		  .queryString("outputFormat", "rapidJSON")
		  .queryString("type_sf", "any")
		  .queryString("version", "10.2.2.48")
		  .asJson().getBody().getObject();
	}
}
